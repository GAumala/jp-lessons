４つの文法があります。

これらはよくにていますね。

まず、使い方の見分け方は <br>
肯定的（こうていてき＝ポジティヴ）<br>
か <br>
否定的（ひていてき＝ネガティブ） <br>
に使えるかどうか見てください。

### 「～げ」

肯定的、否定的に使える <br>
肯定的：あの人は自信ありげだった。<br>
否定的：あの人はさびしげな目をしている

「～げ」はどちらでも使えます。

でも、「がち」「っぽい」「気味」は否定的な文（よくないこと）だけに使えます

「げ」　状態（〇）　性質（✖）<br>
例えば <br>
あの人はさびしげな目をしている。（状態）→〇 <br>
あの人はやさしげな性格だと思う。（性質）→✖ <br>

「がち」状態（〇）性質（✖）<br>
私は子供のころ、病気がちだった（状態）→〇 <br>
私は子供のころ、やさしいがちだった（性質）→✖ <br>

だからこの文法は形容詞は使えません。

「っぽい」　　状態（✖）　　性質（〇）<br>
私は子供のころ、病気っぽかった（状態）→✖ <br>
私は子供のころおこりっぽかった、（性質）→〇<br>

「気味」　　状態（〇）　　性質（✖）<br>
仕事が大変で疲れ気味だ。（状態）→〇<br>
あの人は男なのにピンク色の服が好きだ。女気味だ。（性質）→✖ 

## 練習I

1. a
2. b
3. a
4. a, b
5. a


④について

風邪がち（✖）<br>
「がち」はそうなる数が多いときに使います。<br>
この文は今の状態を話していますので、「がち」は使えません。

熱気味だ（✖）<br>
熱＝性質　だから使えません

## 練習II

6. 4 3 2 1
7. 4 2 1 3
