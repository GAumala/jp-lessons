![](/images/2018-09-12/02.jpg)

「帖」読み方は「じょう」です

「畳」と書く時もあります。どちらも同じ意味です。

「畳」の漢字は？知っていますか？

畳＝たたみ

![](/images/2018-09-12/03.jpg)

畳の部屋です

畳は日本の部屋のゆかです。

スペイン語でgruesa de paja cubierta con un tejido de juncos japonesです


![](/images/2018-09-12/04.gif)

「帖（畳）」の意味の説明に戻りましょう。

日本では部屋の広さを「帖（畳）」で数えます。

1帖、2帖、３帖、、、、

１帖は大体1.62㎡です。

畳１枚の大きさが大体1.62㎡です。

![](/images/2018-09-12/02.jpg)

この写真の間取りは「２LDK」です。

L　リビング <br>
D　ダイニング <br>
K　キッチン <br>

<iframe width="560" height="315" src="https://www.youtube.com/embed/RUq9nx9gin4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/RAv_9wy24ko" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

これは問題ではありません。　おまけです。不動産屋のコマーシャルです。
