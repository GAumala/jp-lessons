宿題の漢字を使った単語の練習をします

### 禁　

この漢字を使った単語を書きますので、読み方を書いてください。

* **解禁**
  
  かいきん levantamiento de la prohibición 

* **監禁**

  かんきん confinamiento, encierro

* **禁止**

  きんし prohibición

* **禁断**

  きんだん prohibido

* **禁物**

  きんもつ　tabú

* **厳禁**

  げんきん　prohibición estricta

初めて聞く単語があったら覚えてください。

### 煙　

読み方は「けむり」、「エン」ですよね

* **煙突**

  えんとつ　chimenea

* **煙道**

  えんどう　conducto de humo

* **煙たがる**

  けむたがる　evitar el trato con uno, mantenerse alejado de. El humo molesta 
  eso es origen de este verbo

### 静

これはたぶんもう知っていますよね。

si tiene 「青」este parte en un KANJI generalmente tiene ONYOMI, [SEI]


例えば、精、清、晴、請　みんな音読み（おんよみ）は「せい」です


* **安静**

  あんせい reposo

* **静止**

  せいし　inmovilizarse, pararse

* **静寂**

  せいじゃく　silencio, quietud （静かで寂しい　→静寂）

* **冷静**

  れいせい　serenidad, calma, sangre fría（冷たくて静か→　冷静）

* **静脈**

  じょうみゃく　vena

### 危

origen de este kanji es UNA PERSONA ESTA AGACHANDOSE EN UN PRECIPICIO, Y ESTA PELIGROSO

危ない　あぶない　peligroso

* **危機**

  きき　crisis

* **危険**

  きけん　peligroso, riesgo

* **危うい**

  あやうい peligroso. （「危うい」は「危うくする」と動詞にもできます。）

* **危ぶまれる**

  あやぶまれる 　は「危ぶむ　あやぶむ　」の受け身（うけみ）です

  危ぶむ　あやぶむ　temer, inquietarse por algo, recelar, preocuparse de algo  

危ぶむ　「父の病状が危ぶまれる」　(Me inquieta (preocupa) mucho la enfermedad de mi padre.

 


















  

