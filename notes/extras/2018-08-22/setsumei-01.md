自動詞、他動詞わかりますか？

自動詞　verbo intransitivo, 他動詞　verbo transitivo

* **伝わるは自動詞ですか？他動詞ですか？**

  伝わるは自動詞です

自動詞　verbo intransitivo　助詞「が」を使います。<br>
他動詞　verbo transitivo　助詞「を」を使います。 

自動詞　verbo intransitivo　助詞「が」を使います。　　まど　が　風　で開く（あく）<br>
他動詞　verbo transitivo　助詞「を」を使います。　私は　まど　を　開ける　（あける）

「が　伝わる」自動詞　verbo intransitivo, 　　「を　伝える」　他動詞　verbo transitivo

* **それではもう一度文を作ってみてください**

  伝統は両親から子供に代々伝わる。

---

<iframe width="560" height="315" src="https://www.youtube.com/embed/6U0CiX_gF8k" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

今から、ニュースの内容について質問します、答えを書いてください。

1. **どこでイベントが開かれていますか。**
     
   東京国際フォーラムで開かれています。

2. **イベントでどんなことができますか。**

   日本の伝統と文化が体験できます。

3. **どんな人たちで賑わっていますか。**

   家族連れや外国人で賑わっています。

4. **イベントの名称は何ですか。**

   J　CULTURE　FESTです。

5. **このイベントは今年で何回目ですか。**

   二回目です。

6. **このイベントの目的は何ですか。**

   日本文化を国内外に広げることです。

7. **このイベントのコンセプトは何ですか。**

   伝統と革新です。

