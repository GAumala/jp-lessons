#!/usr/bin/env bash
set -e
set -o pipefail

ARCHIVE="jp-lessons.tar.gz"

tar -zcvf $ARCHIVE --transform s/_site/jp-lessons/ _site 
scp -i IDENTITY $ARCHIVE REMOTE:~
ssh -i IDENTITY REMOTE tar -xmvf $ARCHIVE
rm $ARCHIVE
