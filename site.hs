--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiWayIf #-}

import           Data.Monoid (mappend)
import           Data.List (isPrefixOf, isSuffixOf, stripPrefix)
import           System.FilePath (combine, joinPath, replaceExtension, takeBaseName)
import           Hakyll


--------------------------------------------------------------------------------
main :: IO ()
main = hakyll $ do
    match ("files/**/*" .||. "fonts/*" .||. "images/*" .||.  "images/**/*") $ do
        route   idRoute
        compile copyFileCompiler

    match "css/*" $ do
        route   idRoute
        compile compressCssCompiler

    match "notes/bunpou/*" $ 
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/bunpou.html" defaultContext
            >>= relativizeUrls

    match "notes/dokkai/*" $ 
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/dokkai.html" defaultContext
            >>= relativizeUrls

    match "notes/kanji/*" $ 
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/kanji.html" defaultContext
            >>= relativizeUrls

    match "notes/extras/**/*" $ do
        compile $ do
            ctx <- extraSectionCtx `fmap` getResourceFilePath
            pandocCompiler
                >>= loadAndApplyTemplate "templates/extra.html" ctx
                >>= relativizeUrls

    match "lessons/*" $ version "id" $ do
        compile $ (makeItem . takeBaseName) 
            =<< getResourceFilePath
        
    match "lessons/*" $ do
        route $ setExtension "html"
        compile $ do
            currentLessonId <- takeBaseName `fmap` getResourceFilePath

            let lessonIdItems = recentFirst 
                                  =<< loadAll ("lessons/*" .&&. hasVersion "id")
            lessonIds <- (map itemBody) `fmap` lessonIdItems
            let lessonPos = findLessonPosition currentLessonId lessonIds

            sectionIds <- getLessonSections `fmap` getResourceBody 
            let sectionItems  = mapM load sectionIds 

            let lessonsContext = lessonIndexCtx currentLessonId
            let ctx = 
                    listField "sections" defaultContext sectionItems  `mappend` 
                    listField "lessons"  lessonsContext lessonIdItems `mappend` 
                    lessonPosCtx lessonPos                            `mappend`
                    lessonCtx

            makeItem ""
                >>= loadAndApplyTemplate "templates/lesson.html" ctx
                >>= relativizeUrls
          
    create ["index.html"] $ do
        route idRoute
        compile $ do      
            latestLessonId:_ <- recentFirst 
                =<< loadAll ("lessons/*" .&&. hasVersion "id")
            let latestLessonUrl = "lessons"                 `combine` 
                                  (itemBody latestLessonId) `mappend` 
                                  ".html"
            makeItem $ Redirect $ latestLessonUrl
                        

    match "templates/*" $ compile templateBodyCompiler


--------------------------------------------------------------------------------
getLessonSections :: Item String -> [Identifier]
getLessonSections item = ((map toId) . lines . itemBody) item
  where lessonDate = (takeBaseName . toFilePath . itemIdentifier) item
        toId = sectionStmntToId lessonDate 

sectionStmntToId :: String -> String -> Identifier
sectionStmntToId lessonDate sectionStmnt 
  | any sectionStmntStartsWith [ "kanji/",  "dokkai/", "bunpou/", "choukai/"] =
    fromFilePath $ 
        "notes" `combine` sectionStmnt `replaceExtension` ".md" 
  | Just pageName <- stripPrefix "extras/" sectionStmnt = 
    fromFilePath $
        joinPath [ "notes/extras", lessonDate, pageName ] `replaceExtension` ".md"
  | otherwise = error $ sectionStmnt <> " does not refer to any known resource."
  where sectionStmntStartsWith x = isPrefixOf x sectionStmnt

data LessonPositon = MidLesson String String 
                   | FirstLesson String 
                   | LastLesson String 
                   | NoLesson

findLessonPositionInTail :: String -> String -> [String] -> LessonPositon
findLessonPositionInTail target next [] = NoLesson
findLessonPositionInTail target next [x] = 
    if target == x then FirstLesson next else NoLesson
findLessonPositionInTail target next [x, y] =
    if | target == x -> MidLesson y next
       | target == y -> FirstLesson x
       | otherwise   -> NoLesson
findLessonPositionInTail target next (x:y:xs) =
    if | target == x -> MidLesson y next
       | target == y -> findLessonPositionInTail target x (y:xs)
       | otherwise   -> findLessonPositionInTail target y xs

-- list is assumed to be sorted by recent first!
findLessonPosition :: String -> [String] ->  LessonPositon
findLessonPosition target [] = NoLesson 
findLessonPosition target [x] = NoLesson 
findLessonPosition target [x, y] = 
    if | target == x -> LastLesson y 
       | target == y -> FirstLesson x 
       | otherwise   -> NoLesson
findLessonPosition target [x, y, z] = 
    if | target == x -> LastLesson y 
       | target == y -> MidLesson z x
       | target == z -> FirstLesson y
       | otherwise   -> NoLesson
findLessonPosition target (x:y:z:xs) = 
    if | target == x -> LastLesson y 
       | target == y -> MidLesson z x
       | target == z -> findLessonPositionInTail target y (z:xs)
       | otherwise   -> findLessonPositionInTail target z xs

lessonPosCtx :: LessonPositon -> Context String
lessonPosCtx (MidLesson prev next) = 
    constField "prev" prev `mappend`
    constField "next" next
lessonPosCtx (FirstLesson next) = constField "next" next
lessonPosCtx (LastLesson prev) = constField "prev" prev
lessonPosCtx NoLesson = missingField
    
lessonCtx :: Context String
lessonCtx =
    dateField "date" "%Y年%m月%d日" `mappend`
    defaultContext

lessonIndexCtx :: String -> Context String
lessonIndexCtx currentLessonId = 
    boolField "current" isCurrentLesson `mappend`
    dateField "date" "%Y年%m月%d日"     `mappend`
    defaultContext
    where isCurrentLesson item = currentLessonId == itemBody item

extraSectionCtx :: FilePath -> Context String
extraSectionCtx filepath = 
    if isSuffixOf "shukudai.md" filepath then
        constField "header" "宿題" `mappend`
        defaultContext
    else
        defaultContext
